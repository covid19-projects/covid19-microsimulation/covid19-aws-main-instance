import os
from src.headless.model_parameter import ModelParameter

class Environment:
    def __init__(self, values):
        self._values = values
    
    @classmethod
    def defined_by(cls, *args, **kwargs):
        return cls(dict((name, ModelParameter.suitable_by_value({'name':name, 'value':value})) for name, value in kwargs.items()))

    def __getitem__(self, arg):
        return self._values[arg].value

    # ACCESSORS
    def values(self):
        return self._values.values()
    
    # AUXS
    def ensure_directory(self, path):
        if not os.path.exists(path):
            os.mkdir(path)
        return path
    
    def path(self, root, *args, ensure=True):
        result = os.path.join(root, *args)
        if ensure:
            return self.ensure_directory(result)
        else:
            return result
        
    # MAIN PATHS AND FILES
    def project_path(self, *args, **kwargs):
        return self.path(self['project_path'], *args, **kwargs)
    
    def general_output_path(self, *args, **kwargs):
        return self.path(self['general_output_path'], *args, **kwargs)
    
    def resources_path(self, *args, **kwargs):
        return self.path(self['resources_path'], *args, **kwargs)

    def output_path(self, timestamp, *args, **kwargs):
        return self.general_output_path(timestamp+'/', *args, **kwargs)