import os
import random
from src.headless.conversions import *
from src.headless.model_parameter import StringParameter, IntegerParameter, FloatParameter, BooleanParameter

class ModelParameters:
    def __init__(self):
        self._values = {}
        self.set_experiment_parameters()
        self.set_epidemiological_parameters()
        self.set_activity_parameters()
        self.set_restriction_parameters()
        self.set_coordinate_system()

    def at(self, model_parameter_name):
        try:
            return self._values[model_parameter_name].value
        except KeyError as error:
            raise KeyError(f'Parameter {model_parameter_name} is not defined in current Model Parameters.')

    def add(self, model_parameter):
        self._values[model_parameter.name] = model_parameter
        return self
    
    def drop(self, parameter_name):
        return self._values.pop(parameter_name, None)

    def values(self):
        return self._values.values()

    # TO SET DURING SIMULATOR CREATION AND CONFIGURATION
    def set_simulation_mode(self, simulation_mode):
        self.add(StringParameter(_name='mode', _value=simulation_mode))
        return self

    def set_extra_parameters_file(self, extra_parameters_file_path):
        self.add(StringParameter(_name='extra_parameters_json_file_path', _value=extra_parameters_file_path))
        return self
    
    def set_output_path(self, output_path):
        self.add(StringParameter(_name='output_path', _value=output_path))
        return self

    def set_xml_headless_file_path(self, xml_headless_file_path):
        self.add(StringParameter(_name='xml_headless_file_path', _value=xml_headless_file_path))
        return self
    
    # TO SET DURING PARAMETERS BUILD
    def set_experiment_parameters(self, individual_amount=20000, final_step=24*7, display_size=30, individual_speed=2, starting_date='2020-03-20 00:00:00', \
                                  time_step_in_minutes=60, steps_between_collect=24, steps_between_dump=24, seed=random.random()):
        self.\
            add(IntegerParameter(_name='individual_amount', _value=individual_amount)).\
            add(IntegerParameter(_name='final_step', _value=final_step)).\
            add(IntegerParameter(_name='display_size', _value=display_size)).\
            add(FloatParameter(_name='individual_speed', _value=individual_speed, _conversion=km_per_hour_to_gama)).\
            add(StringParameter(_name='starting_date', _value=starting_date)).\
            add(FloatParameter(_name='step', _value=time_step_in_minutes, _conversion=minutes_to_seconds)).\
            add(IntegerParameter(_name='steps_between_collect', _value=steps_between_collect)).\
            add(IntegerParameter(_name='steps_between_dump', _value=steps_between_dump)).\
            add(FloatParameter(_name='seed', _value=seed))
        return self
    
    def set_epidemiological_parameters(self, contact_distance_in_meters=2, patient_zero_amount=1, transmission_ratio=0.1, asymptomatic_ratio=0.8, \
                                      mild_ratio=0.80 , severe_ratio=0.15 , icu_ratio=0.05, mean_latent_days=5, st_deviation_latent_days=0.5, \
                                      mean_incubation_days=7, st_deviation_incubation_days=1, mean_mild_recovery_days=11.1 , st_deviation_mild_recovery_days=2 , \
                                      mean_severe_recovery_days=28.6 , st_deviation_severe_recovery_days=3 , mean_icu_recovery_days=29.1 , \
                                      st_deviation_icu_recovery_days=3 , days_on_surfaces=1):
        self.\
            add(FloatParameter(_name='contact_distance', _value=contact_distance_in_meters)).\
            add(IntegerParameter(_name='patient_zero_amount', _value=patient_zero_amount)).\
            add(FloatParameter(_name='transmission_ratio', _value=transmission_ratio)).\
            add(FloatParameter(_name='asymptomatic_ratio', _value=asymptomatic_ratio)).\
            add(FloatParameter(_name='mild_ratio', _value=mild_ratio)).\
            add(FloatParameter(_name='severe_ratio', _value=severe_ratio)).\
            add(FloatParameter(_name='icu_ratio', _value=icu_ratio)).\
            add(FloatParameter(_name='mean_latent_days', _value=mean_latent_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='st_deviation_latent_days', _value=st_deviation_latent_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='mean_incubation_days', _value=mean_incubation_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='st_deviation_incubation_days', _value=st_deviation_incubation_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='mean_mild_recovery_days', _value=mean_mild_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='st_deviation_mild_recovery_days', _value=st_deviation_mild_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='mean_severe_recovery_days', _value=mean_severe_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='st_deviation_severe_recovery_days', _value=st_deviation_severe_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='mean_icu_recovery_days', _value=mean_icu_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='st_deviation_icu_recovery_days', _value=st_deviation_icu_recovery_days, _conversion=days_to_gama)).\
            add(FloatParameter(_name='days_on_surfaces', _value=days_on_surfaces, _conversion=days_to_gama))
        return self
    
    def set_activity_parameters(self, external_workers_amount=3000, essential_workers_amount=10000, supplying_ratio=0.005):
        self.\
            add(IntegerParameter(_name='external_workers_amount', _value=external_workers_amount)).\
            add(IntegerParameter(_name='essential_workers_amount', _value=essential_workers_amount)).\
            add(FloatParameter(_name='supplying_ratio', _value=supplying_ratio))
        return self
    
    def set_restriction_parameters(self, restrictions_compliance_ratio=0.60, people_get_self_quarantine=True, \
                                   quarantine_compliance_ratio=0.75, allow_excepted=True, schools_are_open=False, travel_radius_in_km=10):
        self.\
            add(FloatParameter(_name='restrictions_compliance_ratio', _value=restrictions_compliance_ratio)).\
            add(BooleanParameter(_name='people_get_self_quarantine', _value=people_get_self_quarantine)).\
            add(FloatParameter(_name='self_quarantine_compliance_ratio', _value=quarantine_compliance_ratio)).\
            add(BooleanParameter(_name='allow_excepted', _value=allow_excepted)).\
            add(BooleanParameter(_name='schools_are_open', _value=schools_are_open)).\
            add(IntegerParameter(_name='travel_radius', _value=travel_radius_in_km, _conversion=km_to_meters))
        return self
    
    def set_coordinate_system(self, coordinate_system_input='EPSG:3857', coordinate_system_output='EPSG:4326'):
        self.\
            add(StringParameter(_name='coordinate_system_input', _value=coordinate_system_input)).\
            add(StringParameter(_name='coordinate_system_output', _value=coordinate_system_output))
        return self

    def set_resources_path(self, resources_path):
        self.\
            add(StringParameter(_name='satellital_image_file_path', _value=os.path.join(resources_path,'satellital','satellital.tif'))).\
            add(StringParameter(_name='streets_shapefile_file_path', _value=os.path.join(resources_path,'streets','streets.shp'))).\
            add(StringParameter(_name='routes_shapefile_file_path', _value=os.path.join(resources_path,'routes','routes.shp'))).\
            add(StringParameter(_name='houses_shapefile_file_path', _value=os.path.join(resources_path,'houses','houses.shp'))).\
            add(StringParameter(_name='blocks_shapefile_file_path', _value=os.path.join(resources_path,'blocks','blocks.shp'))).\
            add(StringParameter(_name='workplaces_shapefile_file_path', _value=os.path.join(resources_path,'workplaces','workplaces.shp'))).\
            add(StringParameter(_name='hospitals_shapefile_file_path', _value=os.path.join(resources_path,'hospitals','hospitals.shp'))).\
            add(StringParameter(_name='train_stations_shapefile_file_path', _value=os.path.join(resources_path,'train_stations','train_stations.shp'))).\
            add(StringParameter(_name='frontiers_shapefile_file_path', _value=os.path.join(resources_path,'frontiers','frontiers.shp'))).\
            add(StringParameter(_name='census_radios_shapefile_file_path', _value=os.path.join(resources_path,'census_radio','census_radio.shp')))
        return self