import os
import pandas as pd
from lxml import etree as et
from abc import ABC, abstractmethod
from src.core.environment import Environment
from src.core.scenario import Scenario
from src.experiments.experiment import Experiment
from src.headless.model_parameter import ModelParameter
from src.headless.model_parameters import ModelParameters
from src.tools.suitable_class_finder import SuitableClassFinder

class ExecutionStrategy(ABC):
    def __init__(self, execution):
        self.execution = execution
        self.executed_instances = {}

    @classmethod
    def applied_on(cls, headless_file_path, execution):
        xml_file_tree = et.parse(headless_file_path)
        experiment_mode = cls.find_model_parameter(xml_file_tree.getroot(), 'mode')
        strategy_subclass = SuitableClassFinder(cls).suitable_for(experiment_mode)
        return strategy_subclass(xml_file_tree.getroot(), execution)

    @classmethod
    def can_handle(cls, experiment_mode):
        return experiment_mode in cls.accepted_simulation_modes()

    @classmethod
    @abstractmethod
    def accepted_simulation_modes(cls):
        pass

    @classmethod
    def find_model_parameter(cls, root, parameter_name):
        parameters_tag = root.find('Simulation').find('Parameters')
        for parameter_tag in parameters_tag:
            if parameter_tag.attrib['name'] == parameter_name:
                result = parameter_tag.attrib['value']
                break
        return result

    def get_value_of(self, data_tag_name, data_tags):
        return next(filter(lambda data_tag: data_tag.attrib['name'] == data_tag_name, data_tags)).attrib['value']

    # COMMON ACCESSORS
    @abstractmethod
    def instance(self, simulation_id):
        pass

    # FILES AND PATHS
    @abstractmethod
    def csv_file_path(self, filename, simulation_id):
        pass

class ParallelExecutionStrategy(ExecutionStrategy):
    @classmethod
    def accepted_simulation_modes(cls):
        return ['Parallel']

    def __init__(self, execution_root, execution):
        super(ParallelExecutionStrategy, self).__init__(execution)
        execution_root_path = type(self).find_model_parameter(execution_root, 'output_path')

        for root, dirs, files in os.walk(os.path.dirname(os.path.dirname(execution_root_path))):
            for file in files:
                if file.endswith("headless.xml"):
                    for part in root.split('/'):
                        if part.startswith('parameters-combination'):
                            index = int(part.split('-')[-1])
                    headless_file_path = os.path.join(root, file)
                    SingleExecutionStrategy(et.parse(headless_file_path).getroot(), execution, index=index, wrapper=self)
    
    # COMMON ACCESSORS
    def instance(self, simulation_id):
        return self.executed_instances[simulation_id]

    # FILES AND PATHS
    def csv_file_path(self, filename, simulation_id):
        return self.execution.output_file_path('csv', f'{filename}.csv')
        
class SingleExecutionStrategy(ExecutionStrategy):
    @classmethod
    def accepted_simulation_modes(cls):
        return ['Cloud', 'Local']

    def __init__(self, execution_root, execution, index=0, wrapper=None):
        super(SingleExecutionStrategy, self).__init__(execution)
        if not wrapper:
            wrapper = self
        simulation_tag = execution_root.find('Simulation') # <--- XML FILE SHOULD HAS JUST ONE SIMULATION TAG (USE CLOUD FOR MULTIPLE PARALLEL SIMULATIONS)

        # PARSE METADATA
        attributes = simulation_tag.attrib
        id = int(attributes['id'])
        source_path = attributes['sourcePath']      # <--- NOT USED IN PYTHON CLIENT SIDE, ITS JUST FOR GAMA
        experiment_name = attributes['experiment']  # <--- ALREADY SET ON EXCLUSIVE EXPERIMENT DATA
        final_step = int(attributes['finalStep'])   # <--- ITS IN THE MODEL PARAMETERS TOO

        # PARSE MODEL PARAMETERS
        model_parameters = ModelParameters()
        parameters_tag = simulation_tag.find('Parameters')
        for parameter_tag in parameters_tag:
            model_parameters.add(ModelParameter.suitable_by_type(parameter_tag.attrib))

        # PARSE EXCLUSIVE ENVIRONMENT DATA
        environment_tag = simulation_tag.find('Environment')
        data_dict = dict((data_tag.attrib['name'], ModelParameter.suitable_by_type(data_tag.attrib)) for data_tag in environment_tag)
        environment = Environment(data_dict)

        # PARSE EXCLUSIVE EXPERIMENT DATA
        experiment_tag = simulation_tag.find('Experiment')
        data_dict = dict((data_tag.attrib['name'], ModelParameter.suitable_by_type(data_tag.attrib)) for data_tag in experiment_tag)
        experiment = Experiment.suitable_for(data_dict)
        
        # PARSE EXCLUSIVE SCENARIO DATA
        scenario_tag = simulation_tag.find('Scenario')
        data_tags = [data_tag for data_tag in scenario_tag]
        timestamp = self.get_value_of('timestamp', data_tags)
        scenario_name = self.get_value_of('scenario_name', data_tags)
        scenario = Scenario(name=scenario_name, model_parameters=model_parameters, environment=environment)

        # STORE PARSED SIMULATION
        wrapper.executed_instances[index] = ExecutedInstance(timestamp, experiment, scenario)

    # COMMON ACCESSORS
    def instance(self, simulation_id):
        return self.executed_instances[0]

    # FILES AND PATHS
    def csv_file_path(self, filename, simulation_id):
        return self.execution.output_file_path('csv', f'Simulation {simulation_id}', f'{filename}.csv')

class ExecutedInstance:
    def __init__(self, timestamp, experiment, scenario):
        self.timestamp = timestamp
        self.experiment = experiment
        self.scenario = scenario