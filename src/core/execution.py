import os
import pandas as pd
from datetime import datetime
from src.core.simulator import AbstractSimulator
from src.interfaces.execution_strategy import ExecutionStrategy
from src.interfaces.population_snapshot_dumper import PopulationSnapshotDumper
from src.analysis.plotting import BasicSEIR, DetailedSEIR, Activities, R0, KeplerGL

class Execution:
    @classmethod
    def defined_by(cls, execution_path):
        execution = cls()
        return execution.configure(ExecutionStrategy.applied_on(execution_path, execution))

    def configure(self, execution_strategy):
        self.strategy = execution_strategy
        return self

    def resume_simulation(self, last_timestamp, simulation_id=0, **kwargs):
        self.merge_cases_evolution_files(simulation_id=simulation_id)
        dumper = PopulationSnapshotDumper(self, simulation_id=simulation_id)
        dumper.start_dumping()

        finish_date = datetime.strptime(last_timestamp, '%Y-%m-%d %H:%M:%S')
        starting_date = datetime.strptime(dumper.last_timestamp, '%Y-%m-%d %H:%M:%S')
        simulator = AbstractSimulator.suitable_for(self)
        experiment = self.experiment()
        return simulator.simulate(
            experiment.resume_with(
                final_step = int((finish_date - starting_date).days * self.cycles_per_day(simulation_id)),
                starting_date = dumper.last_timestamp,
                snapshot_file_path = dumper.snapshot_file_path,
                **kwargs))

    def merge_cases_evolution_files(self, simulation_id=0):
        try:
            first_simulation_directory = os.path.dirname(self.experiment(simulation_id).at('snapshot_file_path'))
            first_cases_evolution = self.load_csv(os.path.join(first_simulation_directory, 'cases_evolution.csv'))
            for simulation_id in range(len(os.listdir(self.output_file_path('csv', simulation_id=simulation_id)))):
                new_cases_evolution = self.load_csv_file_for('cases_evolution', simulation_id=simulation_id)
                merged_cases_evolution = first_cases_evolution.append(new_cases_evolution, ignore_index=True)
                self.save_dataframe_as_csv_file(merged_cases_evolution, 'cases_evolution', simulation_id=simulation_id)
        except KeyError:
            print('The current execution doesn\'t have the \'snapshot_file_path\' parameter configured. It doesn\'t seems to be a previous resumed simulation and there is nothing to merge. Simulation resumes normally.')

    # COMMON ACCESSORS
    def scenario(self, simulation_id=0):
        return self.strategy.instance(simulation_id).scenario

    def environment(self, *args, simulation_id=0):
        return self.scenario(simulation_id=simulation_id).environment(*args)
    
    def model_parameters(self, *args, simulation_id=0):
        return self.scenario(simulation_id=simulation_id).model_parameters(*args)

    def timestamp(self, simulation_id=0):
        return self.strategy.instance(simulation_id).timestamp
    
    def experiment(self, simulation_id=0):
        return self.strategy.instance(simulation_id).experiment

    def cycles_per_day(self, simulation_id=0):
        seconds_per_day = 60*60*24
        return seconds_per_day // self.model_parameters(simulation_id=simulation_id).at('step')

    # FILES AND PATHS
    def csv_file_path(self, filename, simulation_id=0):
        return self.strategy.csv_file_path(filename, simulation_id)

    def load_csv(self, csv_file_path, separator=';'):
        return pd.read_csv(csv_file_path, sep=separator)

    def load_csv_file_for(self, filename, simulation_id=0):
        return self.load_csv(self.csv_file_path(filename, simulation_id=simulation_id))

    def save_dataframe_as_csv_file(self, dataframe, filename, simulation_id=0):
        csv_file_path = self.csv_file_path(filename, simulation_id=simulation_id)
        dataframe.to_csv(csv_file_path, sep=';', index=False)
        return csv_file_path

    def output_file_path(self, *args, simulation_id=0):
        return os.path.join(self.model_parameters(simulation_id=simulation_id).at('output_path'), *args)

    # PLOTTING
    def plot_basic_SEIR(self, simulation_id=0):
        return BasicSEIR(self, simulation_id=simulation_id).execute()
    
    def plot_detailed_SEIR(self, simulation_id=0):
        return DetailedSEIR(self, simulation_id=simulation_id).execute()
    
    def plot_activities(self, simulation_id=0):
        return Activities(self, simulation_id=simulation_id).execute()
    
    def plot_R0(self, simulation_id=0, **kwargs):
        return R0(self, simulation_id=simulation_id, **kwargs).execute()
    
    def plot_parameter_optimization(self, simulation_id=0):
        return ParameterOptimization(self, simulation_id=simulation_id).execute()
    
    # KEPLER GL
    def launch_keplergl(self, simulation_id=0, save_to_html=False):
        return KeplerGL(self, simulation_id=simulation_id, save_to_html=save_to_html).execute()