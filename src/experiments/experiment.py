import copy
from abc import ABC, abstractmethod
from src.tools.suitable_class_finder import SuitableClassFinder
from src.headless.model_parameter import ModelParameter

# ABSTRACT CLASS
class Experiment(ABC):
    def __init__(self, *args, **kwargs):
        self.custom_parameters = {}
        for parameter in kwargs.values():
            self.add(parameter)

    # ACCESSORS
    def at(self, parameter_name):
        try:
            return self.custom_parameters[parameter_name].value
        except KeyError as error:
            raise KeyError(f'Parameter {parameter_name} is not defined in current Experiment.')

    def values(self):
        return self.custom_parameters.values()

    def resume_with(self, **kwargs):
        new_experiment = copy.deepcopy(self)
        for name, value in kwargs.items():
            new_experiment.add(ModelParameter.suitable_by_value({'name':name, 'value':value}))
        return new_experiment

    # SUITABLE EXPERIMENT
    @classmethod
    def suitable_for(cls, data_dict):
        experiment_subclass = SuitableClassFinder(cls).suitable_for(data_dict.pop('name').value)
        return experiment_subclass(**data_dict)

    @classmethod
    def can_handle(cls, a_experiment_name):
        return cls.experiment_name() == a_experiment_name

    @classmethod
    @abstractmethod
    def experiment_name(cls):
        pass

    @classmethod
    def experiment_name_parameter(cls):
        return ModelParameter.suitable_by_value({'name':'name', 'value':cls.experiment_name()})

    # CONFIGURATION
    def add(self, custom_parameter):
        self.custom_parameters[custom_parameter.name] = custom_parameter
        return self 

    def configure_on(self, headless_specification, xml_simulation_element):
        parameters_to_update = ['starting_date', 'final_step']
        for parameter_name in parameters_to_update:
            try:
                headless_specification.simulation.model_parameters().add(self.custom_parameters[parameter_name])
            except KeyError as error:
                pass

# TYPES OF EXPERIMENTS
class GUIExperiment(Experiment):
    def __init__(self, *args, **kwargs):
        super(GUIExperiment, self).__init__(*args, **kwargs)
        
    def configure_on(self, headless_specification, xml_simulation_element):
        super(GUIExperiment, self).configure_on(headless_specification, xml_simulation_element)
        headless_specification.add_full_header_to(xml_simulation_element, self.at('name'))
        
class BatchExperiment(Experiment):
    def __init__(self, *args, **kwargs):
        super(BatchExperiment, self).__init__(*args, **kwargs)
        self.parameters_to_remove = ['transmission_ratio', 'asymptomatic_ratio', 'supplying_ratio', 'restrictions_compliance_ratio', 'self_quarantine_compliance_ratio']

    def configure_on(self, headless_specification, xml_simulation_element):
        super(BatchExperiment, self).configure_on(headless_specification, xml_simulation_element)
        for each in self.parameters_to_remove:
            headless_specification.simulation.model_parameters().drop(each)
        headless_specification.add_header_to(xml_simulation_element, self.at('name'))

# EXPERIMENTS
class BuildingsViralLoad(GUIExperiment):
    def __init__(self, *args, **kwargs):
        super(BuildingsViralLoad, self).__init__(*args, name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'BuildingsViralLoad'

class GIS_Movements(GUIExperiment):
    def __init__(self, *args, **kwargs):
        super(GIS_Movements, self).__init__(*args,  name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'GIS_Movements'

class HospitalCapacity(GUIExperiment):
    def __init__(self, *args, **kwargs):
        super(HospitalCapacity, self).__init__(*args,  name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'HospitalCapacity'

class MonitorsExperiment(GUIExperiment):
    def __init__(self, *args, **kwargs):
        super(MonitorsExperiment, self).__init__(*args,  name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'MonitorsExperiment'
        
class ParameterExploration(BatchExperiment):
    def __init__(self, *args, **kwargs):
        super(ParameterExploration, self).__init__(*args,  name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'ParameterExploration'
        
class ParameterOptimization(BatchExperiment):
    def __init__(self, *args, **kwargs):
        super(ParameterOptimization, self).__init__(*args,  name=type(self).experiment_name_parameter(), **kwargs)

    @classmethod
    def experiment_name(cls):
        return 'ParameterOptimization'