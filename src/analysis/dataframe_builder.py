import pandas as pd
from abc import ABC, abstractmethod

class DataframeBuilder(ABC):
    def __init__(self, plotter):
        self.plotter = plotter

    @abstractmethod
    def build(self):
        pass

class MonitorDataframe(DataframeBuilder):
    def build(self):
        monitors = self.plotter.execution.load_csv_file_for("monitors", simulation_id=self.plotter.simulation_id)
        dataframe = pd.DataFrame({}, columns=self.plotter.colors_by_columns.keys())
        for column, values in self.plotter.colors_by_columns.items():
            dataframe[column] = monitors[values[0]]
        dataframe["Ciclos"] = monitors["Cycle"] / self.plotter.execution.cycles_per_day()
        return dataframe

class StatesEvolutionDataframe(DataframeBuilder):
    def build(self):
        states_evolution = self.plotter.execution.load_csv_file_for("states_evolution", simulation_id=self.plotter.simulation_id)
        states_evolution["Ciclos"] = states_evolution["current_cycle"] / self.plotter.execution.cycles_per_day(self.plotter.simulation_id)
        return states_evolution

class CasesEvolutionDataframe(DataframeBuilder):
    def build(self):
        cases_evolution = self.plotter.execution.load_csv_file_for("cases_evolution", simulation_id=self.plotter.simulation_id)
        cases_evolution["Ciclos"] = cases_evolution["current_cycle"] / self.plotter.execution.cycles_per_day(self.plotter.simulation_id)
        return cases_evolution