import os
import ast
import pandas as pd
import plotly.graph_objects as go
from tqdm import *
from datetime import datetime, timedelta
from keplergl import KeplerGl
from plotly.subplots import make_subplots
from abc import ABC, abstractmethod
from src.analysis.dataframe_builder import MonitorDataframe, StatesEvolutionDataframe, CasesEvolutionDataframe

# PLOTTING
class Plotter(ABC):
    def __init__(self, execution, simulation_id=0):
        self.execution = execution
        self.simulation_id = simulation_id
        self.colors_by_columns = {
            "Día": ("Datetime", "white"),
            "Ciclos": ("Cycle", "white")}

    @abstractmethod
    def execute(self):
        pass
    
    def build_plot(self, dataframe, title="", col_legend="", secondary_y=False, yaxis=None, xaxis_title="Días", yaxis_title="Cantidad"):
        plot = make_subplots(specs=[[{"secondary_y": secondary_y}]])
        data_columns = list(dataframe.columns)
        data_columns.remove("Ciclos")
        data_columns.remove("Día")
        for column in data_columns:
            plot.add_trace(
                go.Scatter(
                    x=list(dataframe["Día"]), 
                    y=list(dataframe[column]), 
                    name=column,
                    marker=dict(color=self.colors_by_columns[column][1])), 
                secondary_y=(column == secondary_y))

        plot.update_layout(title=title, xaxis_title=xaxis_title, yaxis_title=yaxis_title, legend_title=col_legend, yaxis=yaxis)
        return plot

class BasicSEIR(Plotter):
    def __init__(self, *args, **kwargs):
        super(BasicSEIR, self).__init__(*args, **kwargs)
        self.colors_by_columns.update([
            ("Susceptibles", ("#Susceptible", "green")),
            ("Expuestos", ("#Exposed", "gold")),
            ("Infecciosos", ("#Infectious", "red")),
            ("Recuperados", ("#Recovered", "lightblue")),
            ("Muertos", ("#Dead", "grey"))])

    def execute(self):
        dataframe = MonitorDataframe(self).build()
        plot = self.build_plot(dataframe, title="Modelo SEIR Básico", col_legend="Estados")
        return dataframe, plot
    
class DetailedSEIR(Plotter):
    def __init__(self, *args, **kwargs):
        super(DetailedSEIR, self).__init__(*args, **kwargs)
        self.colors_by_columns.update([
            ("Susceptibles", ("#Susceptible", "green")),
            ("Expuestos", ("#Exposed", "gold")),
            ("Asintomáticos", ("#Asymptomatic", "orange")),
            ("Presintomáticos", ("#Presymptomatic", "orangered")),
            ("Sintomáticos Leves", ("#Mild_Symptomatic", "red")),
            ("Sintomáticos Severos", ("#Severe_Symptomatic", "brown")),
            ("Sintomáticos ICU", ("#ICU_Symptomatic", "darkred")),
            ("Total Sintomáticos", ("#Symptomatic", "darkred")),
            ("Total Infectados", ("#Infected", "crimson")),
            ("Total Infecciosos", ("#Infectious", "red")),
            ("Recuperados", ("#Recovered", "lightblue")),
            ("Muertos", ("#Dead", "grey"))])

    def execute(self):
        dataframe = MonitorDataframe(self).build()
        plot = self.build_plot(dataframe, title="Modelo SEIR Detallado", col_legend="Estados")
        return dataframe, plot

class Activities(Plotter):
    def __init__(self, *args, **kwargs):
        super(Activities, self).__init__(*args, **kwargs)
        self.colors_by_columns.update([
            ("Descansando", ("#Resting", "green")),
            ("En Movimiento", ("#Moving", "gold")),
            ("Trabajando", ("#Working", "orange")),
            ("Estudiando", ("#Studying", "orangered")),
            ("Fuera del mapa", ("#OutOfMap", "red")),
            ("En Cuarentena", ("#Quarantine", "brown")),
            ("En Tratamiento", ("#Treatment", "darkred")),
            ("Muertos", ("#Death", "grey"))])

    def execute(self):
        dataframe = MonitorDataframe(self).build()
        plot = self.build_plot(
            dataframe, 
            title="Actividades díarias", 
            col_legend="Actividades", 
            yaxis=dict(type='log', range=[1, 6], ticksuffix='#'),
            yaxis_title="Cantidad (Log)")
        return dataframe, plot
    
class R0(Plotter):
    def __init__(self, *args, **kwargs):
        self.window_size_in_days = kwargs.pop('window_size_in_days', 15)
        self.scale = kwargs.pop('scale', 'linear')
        super(R0, self).__init__(*args, **kwargs)
        self.colors_by_columns.update([
            ("R0", ("R0", "tomato")),
            ("Sintomáticos Activos (Tx. Comunitaria)", ("Sintomáticos Activos (Tx. Comunitaria)", "gold"))])

    def execute(self):
        states_evolution_df = StatesEvolutionDataframe(self).build()
        cases_evolution_df = CasesEvolutionDataframe(self).build()

        starting_date = self.execution.model_parameters().at('starting_date')
        starting_date = datetime.strptime(starting_date, '%Y-%m-%d %H:%M:%S')
        final_step = self.execution.model_parameters().at('final_step')
        cycles_per_day = self.execution.cycles_per_day(self.simulation_id)
        amount_of_days = int(final_step / cycles_per_day)
        date_list = [starting_date + timedelta(days=x) for x in range(amount_of_days)]

        # Filtro las transiciones de contagios
        states_evolution_df.drop(states_evolution_df.loc[states_evolution_df['state'] == 'susceptible'].index, inplace = True)
        # Hago el join con la información de los casos (origen y quien lo infectó)
        merged_df = pd.merge(left=states_evolution_df, right=cases_evolution_df, how='left', left_on='person', right_on='new_case')
        # Elimino columnas que no me interesan y renombro las nuevas agregadas
        del merged_df['location_case']
        del merged_df['current_date_y']
        del merged_df['current_cycle_y']
        del merged_df['new_case']
        merged_df.rename(columns={'current_date_x':'current_date', 'current_cycle_x':'current_cycle', 'disease_case':'origin', 'source_case':'infected_by'}, inplace=True)
        merged_df

        r0_series = []
        local_symptomatic_series = []
        
        days = range(0, amount_of_days)
        for day in tqdm(days):
            start_window = day-self.window_size_in_days
            if start_window < 0:
                start_cycle = 0
            else:
                start_cycle = start_window*cycles_per_day
            end_cycle = day*cycles_per_day

            current_local_symptomatic_individuals = set(merged_df.loc[(merged_df['current_cycle'] < end_cycle) & (merged_df['origin'] == 'Local case') & ((merged_df['state'] == 'mild_symptomatic') | (merged_df['state'] == 'severe_symptomatic') | (merged_df['state'] == 'icu_symptomatic'))]['person'].unique())
            current_recovered_individuals = set(merged_df.loc[(merged_df['current_cycle'] < end_cycle) & (merged_df['state'] == 'recovered')]['person'].unique())
            local_symptomatic_series.append(len(current_local_symptomatic_individuals-current_recovered_individuals))
            
            current_r0_individuals_df = merged_df.loc[(merged_df['current_cycle'] >= start_cycle) & (merged_df['current_cycle'] < end_cycle) & ((merged_df['state'] == 'recovered') | (merged_df['state'] == 'dead'))]
            individuals_filtered = len(current_r0_individuals_df)
            if individuals_filtered == 0:
                r0_series.append(0)
            else:
                new_infections = current_r0_individuals_df['infections'].sum(axis=0, skipna=True)
                r0_series.append(new_infections/individuals_filtered)

        rows = []
        for _day, _r0, _current_local_symptomatic in zip(date_list, r0_series, local_symptomatic_series):
            rows.append({
                "Día": _day,
                "R0": _r0,
                "Sintomáticos Activos (Tx. Comunitaria)": _current_local_symptomatic})

        new_r0_df = pd.DataFrame(rows, columns = self.colors_by_columns.keys())
        devolver = new_r0_df
        plot = self.build_plot(
            new_r0_df, 
            title=f"R0 vs. Sintomáticos Activos (Moving Average con ventana de {self.window_size_in_days} días)", 
            col_legend="Series",
            secondary_y=True)
        
        plot.add_shape(
            type='line', x0=date_list[0], y0=1, x1=date_list[-1], y1=1,
            line=dict(color='grey', dash="dot"), xref='x', yref='y')
        
        plot.update_yaxes(title_text="<b>Primario</b> R0", secondary_y=False)
        plot.update_yaxes(title_text="<b>Secundario</b> Sintomáticos Activos", secondary_y=True, type=self.scale)
        return devolver, plot
        #-------------------
        # plot = self.build_plot(dataframe, title="R0 vs. Total Infectados", col_legend="Series", secondary_y="Total Infectados")
        # plot.update_yaxes(title_text="<b>Primario</b> R0", secondary_y=False)
        # plot.update_yaxes(title_text="<b>Secundario</b> Total Infectados", secondary_y=True)
        # return dataframe, plot

# class ParameterOptimization(Plotter):
#     def __init__(self, *args, **kwargs):
#         super(ParameterOptimization, self).__init__(*args, **kwargs)
#         self.real_df = kwargs.get('real_df')
    
#     def execute(self):
#         simulated_dfs = {}
#         plot_real_vs_sim = make_subplots(rows=1, cols=1)
#         simulations_amount = self.execution."simulations_amount"
#         first_day_to_compare = self.execution."first_day_to_compare"
#         cycles_per_day = self.execution.cycles_per_day()

#         for index in tqdm(range(simulations_amount)):
#             filtered_rows = []
#             selected_df = self.execution.load_csv_file_for('monitors', simulation_id=index)
#             temp_df = selected_df.loc[selected_df['Cycle'].between(first_day_to_compare*cycles_per_day, (first_day_to_compare+days_to_compare)*cycles_per_day)]

#             for i, row in temp_df.iterrows():
#                 if isclose(row["Cycle"] % 24, 0, rel_tol=1e-6):
#                     filtered_rows.append(row)
#             temp_df = pd.DataFrame(filtered_rows, columns = selected_df.columns)

#             # Comienzo a generar el dataframe actual
#             simulated_df = pd.DataFrame({}, columns=["Infectados", "Recuperados", "Muertos"])
#             simulated_df["Infectados"] = temp_df["#Symptomatic"]
#             simulated_df["Recuperados"] = temp_df["#Recovered"]
#             simulated_df["Muertos"] = temp_df["#Dead"]
#             simulated_dfs[index] = simulated_df

#             plot_real_vs_sim.add_trace(
#                 go.Scatter(
#                     visible=False,
#                     line=dict(color="orangered", width=3),
#                     name="Infectados (sim. id) = " + str(index),
#                     x=date_list[:days_to_compare+1],
#                     y=list(simulated_df["Infectados"])))
#             plot_real_vs_sim.add_trace(
#                 go.Scatter(
#                     visible=False,
#                     line=dict(color="royalblue", width=3),
#                     name="Recuperados (sim. id) = " + str(index),
#                     x=date_list[:days_to_compare+1],
#                     y=list(simulated_df["Recuperados"])))
#             plot_real_vs_sim.add_trace(
#                 go.Scatter(
#                     visible=False,
#                     line=dict(color="dimgray", width=3),
#                     name="Muertos (sim. id) = " + str(index),
#                     x=date_list[:days_to_compare+1],
#                     y=list(simulated_df["Muertos"])))

#         plot_real_vs_sim.add_trace(go.Scatter(
#             x=date_list[:days_to_compare+1], y=list(real_df["Activos"]), name="Infectados (real.)", line=dict(color='lightsalmon', width=3)))
#         plot_real_vs_sim.add_trace(go.Scatter(x=date_list[:days_to_compare+1], y=list(real_df["Altas"]), name="Recuperados (real.)", line=dict(color="lightskyblue", width=3)))
#         plot_real_vs_sim.add_trace(go.Scatter(x=date_list[:days_to_compare+1], y=list(real_df["Óbitos"]), name="Muertos (real.) ", line=dict(color="darkgray", width=3)))

#         # Hago visible las 3 primeras curvas (las de los valores reales ya estan visibles)
#         plot_real_vs_sim.data[0].visible = True
#         plot_real_vs_sim.data[1].visible = True
#         plot_real_vs_sim.data[2].visible = True

#         # Creo el slider
#         steps = []
#         for i in tqdm(range(simulations_amount)):
#             simulations_data = execution.load_csv_file_for('parameter_optimization', simulation_id=i)
#             step = dict(
#                 method="update",
#                 args=[{"visible": [False] * len(plot_real_vs_sim.data)},
#                     {"title": f"Realidad vs. Simulación ({first_day_of_reality.strftime('%d/%m/%Y')} al {last_day_to_compare.strftime('%d/%m/%Y')}) - Simulación {i} - [%Tx: {simulations_data['transmission_ratio'][0]}, %Asint: {simulations_data['asymptomatic_ratio'][0]}, %Acatamiento: {simulations_data['restrictions_compliance_ratio'][0]}, %AutoCuarentena: {simulations_data['self_quarantine_compliance_ratio'][0]}]"}],  # layout attribute
#             )
#             step["args"][0]["visible"][3*i] = True
#             step["args"][0]["visible"][(3*i)+1] = True
#             step["args"][0]["visible"][(3*i)+2] = True
#             step["args"][0]["visible"][3*simulations_amount] = True
#             step["args"][0]["visible"][(3*simulations_amount)+1] = True
#             step["args"][0]["visible"][(3*simulations_amount)+2] = True
#             steps.append(step)

#         sliders = [dict(
#             active=0,
#             currentvalue={"prefix": "Sim ID: "},
#             pad={"t": 50},
#             steps=steps
#         )]

#         simulations_data_0 = execution.load_csv_file_for('parameter_optimization', simulation_id=0)
#         plot_real_vs_sim.update_layout(
#             title=f"Realidad vs. Simulación ({first_day_of_reality.strftime('%d/%m/%Y')} al {last_day_to_compare.strftime('%d/%m/%Y')}) - Simulación 0 - [%Tx: {simulations_data_0['transmission_ratio'][0]}, %Asint: {simulations_data_0['asymptomatic_ratio'][0]}, %Acatamiento: {simulations_data_0['restrictions_compliance_ratio'][0]}, %AutoCuarentena: {simulations_data_0['self_quarantine_compliance_ratio'][0]}]",
#             yaxis_title="Individuos",
#             xaxis_title="Días",
#             legend_title="Series",
#             xaxis_tickangle=0,
#             xaxis_tickformat = '%d/%m/%Y',
#             yaxis_range=[0,120],
#             sliders=sliders)

# KEPLERGL
class KeplerGL:
    def __init__(self, execution, simulation_id=0, save_to_html=False):
        self.execution = execution
        self.simulation_id = simulation_id
        self.save_to_html = save_to_html

    def execute(self):
        configuration_file = open(self.execution.environment('keplergl_configuration_file', simulation_id=self.simulation_id), 'r')
        contents = configuration_file.read()
        configuration = ast.literal_eval(contents)
        configuration_file.close()

        kepler_map = KeplerGl(height = 800, config=configuration)

        all_csv = []
        all_csv.append(("individuals", self.execution.load_csv_file_for("individuals", simulation_id=self.simulation_id)))
        all_csv.append(("vehicles", self.execution.load_csv_file_for("vehicles", simulation_id=self.simulation_id)))
        all_csv.append(("buildings", self.execution.load_csv_file_for("buildings", simulation_id=self.simulation_id)))
        all_csv.append(("frontiers", self.execution.load_csv_file_for("frontiers", simulation_id=self.simulation_id)))
        all_csv.append(("hospitals", self.execution.load_csv_file_for("hospitals", simulation_id=self.simulation_id)))
        all_csv.append(("routes", self.execution.load_csv_file_for("routes", simulation_id=self.simulation_id)))
        all_csv.append(("streets", self.execution.load_csv_file_for("streets", simulation_id=self.simulation_id)))
        all_csv.append(("census_radios", self.execution.load_csv_file_for("census_radios", simulation_id=self.simulation_id)))
        all_csv.append(("houses", self.execution.load_csv_file_for("houses", simulation_id=self.simulation_id)))
        #all_csv.append(("hospital_capacity", self.execution.load_csv_file_for("hospital_capacity", simulation_id=self.simulation_id)))
        #all_csv.append(("houses_and_disease", self.execution.load_csv_file_for("houses_and_disease", simulation_id=self.simulation_id)))

        for csv_dataframe in all_csv:
            kepler_map.add_data(data=csv_dataframe[1], name=csv_dataframe[0])

        if self.save_to_html:
            html_path = self.execution.environment('html_path')
            html_name = f'{self.execution.scenario(simulation_id=self.simulation_id).name}_{self.execution.timestamp(simulation_id=self.simulation_id)}_{str(self.simulation_id)}.html'
            kepler_map.save_to_html(file_name=os.path.join(html_path, html_name))
        return kepler_map